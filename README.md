# OA AST 2017 - TP1

TP 1 for the ECE OA AST class. This project has been done with the node version v8.4.0. 

## Installation instruction 

Clone the project from gitLab

## Run 

Command line : 
* Use 'npm start' to start the project
* Then enter "curl localhost:8888" or go to "http://localhost:8888" in your web browser 

## Test

Run 'npm test' to run the tests. These tests are executed on GitLab. 

## Contributor

Michelle Deng

## License 

MIT
