should = require('should')
users = require('../src/users.coffee')

describe 'users', () -> 
	it 'Saved successfully', (done) ->
		users.save "name", "password", (err) ->
			should.not.exist err
			done()

	it 'Save failure - Missing parameters', (done) ->
		users.save "name", (err) ->
			should.exist err
			done()

	it 'Get successfully', (done) ->
		users.get "name", (err) ->
			should.not.exist err
			done()

	it 'Updated successfully', (done) ->
		users.save "name", "newPassword", (err) ->
			should.not.exist err
			done()

	it 'Update failure - Missing parameters', (done) ->
		users.save "name", (err) ->
			should.exist err
			done()
