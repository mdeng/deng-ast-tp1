module.exports = 
	save : (name, password, callback) ->
		if callback == undefined
			callback = password
			callback new Error "Save failure - Missing parameters"
		else 
			callback()		

	get : (name, callback) ->
		callback()

	updatePassword : (name, newPassword, callback) ->
		if callback == undefined
			callback = newPassword
			callback new Error "Update failure - Missing parameters"
		else 
			callback()			
