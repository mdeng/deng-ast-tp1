users = require './users.coffee'
fs = require 'fs'

renderResource = (filename, type, res, callback) ->
    console.log "rendering resource #{filename}"
    fs.readFile "public/#{type}/#{filename}"
    , (err, html) ->
      throw err if err 
      res.writeHead 200, 
        'Content-Type': "text/#{type}"
      res.write html
      res.end()

module.exports = 
  
    logic: (req, res) ->

      		if req.url == "/save" 
  		    	users.save "userName", "userPassword", () ->
  		      		res.writeHead 201, 
                  'Content-Type': 'text/plain'
  		      		res.end 'User is saved !'

      		else if req.url == "/get"
      			users.get "userName", () ->
			        res.writeHead 200, 
                'Content-Type': 'text/plain'
			        res.end 'Got the user !'
      			
      		else if req.url == "/update"
      			users.updatePassword "userName", "newPassword", () ->
      				res.writeHead 200, 
                'Content-Type': 'text/plain'
      				res.end 'The password has been updated'
      			
      		else if req.url == "/"
			      renderResource "index.html", "html", res     			
      		else 
      			res.writeHead 404, 
              'Content-Type': 'text/plain'
      			res.end 'Route not found !'

    port: "8888" 
    address: "127.0.0.1"

