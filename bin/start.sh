#!/bin/bash

echo "Compiling coffee files"
./node_modules/.bin/coffee --compile --output lib src

echo "Compile pug files"
./node_modules/.bin/pug views --out public/html --pretty

mkdir public/css
echo "Compile stylus files"
./node_modules/.bin/stylus fonts --out public/css 

#node src/app.js
echo "Starting ..."
node_modules/.bin/coffee src/app.coffee
